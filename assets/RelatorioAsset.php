<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RelatorioAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
        'minimalliteadmin/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css',
        'minimalliteadmin/css/master_style.css',
        'minimalliteadmin/css/skins/_all-skins.css',
        '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
    ];
    public $js = [
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
        'minimalliteadmin/assets/vendor_components/jquery-ui/jquery-ui.js',
        'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js',
        'minimalliteadmin/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js',
        'minimalliteadmin/assets/vendor_components/fastclick/lib/fastclick.js',
        'minimalliteadmin/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.min.js',
        'minimalliteadmin/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-uk-mill-en.js',
        'minimalliteadmin/js/template.js',
        'minimalliteadmin/js/inicio.js',
        'minimalliteadmin/js/demo.js',
        'js/index.js',
        
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
