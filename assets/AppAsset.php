<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'creative/vendor/bootstrap/css/bootstrap.css',
        'creative/vendor/bootstrap/css/bootstrap.min.css',
        'creative/vendor/fontawesome-free/css/all.min.css',
//        'https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800',
//        'https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic',
        'creative/vendor/magnific-popup/magnific-popup.css',
        'creative/css/creative.css',
    ];
    public $js = [
        'creative/vendor/jquery/jquery.min.js',
        'creative/vendor/bootstrap/js/bootstrap.bundle.min.js',
        'creative/vendor/jquery-easing/jquery.easing.min.js',
        'creative/vendor/scrollreveal/scrollreveal.min.js',
        'creative/vendor/magnific-popup/jquery.magnific-popup.min.js',
        'creative/js/creative.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
