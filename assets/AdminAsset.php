<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'minimalliteadmin/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css',
        'minimalliteadmin/assets/vendor_components/bootstrap/dist/css/bootstrap-extend.css',
        'minimalliteadmin/css/master_style.css',
        'minimalliteadmin/css/skins/_all-skins.css',
    ];
    public $js = [
//        'minimalliteadmin/assets/vendor_components/jquery/dist/jquery.min.js',
//        'minimalliteadmin/assets/vendor_components/popper/dist/popper.min.js',
//        'minimalliteadmin/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
