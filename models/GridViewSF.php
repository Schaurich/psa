<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\base\Config;

class GridViewSF extends \kartik\grid\GridView
{
	public $toolbar = [
        '{prepareExport}',
        '{export}',
    ];

    /**
     * @var boolean whether to enable toggling of grid data. Defaults to `true`.
     */
    public $prepareExport = true;

    public $prepareExportOptions = [
            'all' => [
                'icon' => 'resize-full',
                'label' => 'Preparar Exportação',
                'class' => 'btn btn-default',
                'title' => 'Preparar Exportação'
            ],
            'page' => [
                'icon' => 'resize-small',
                'label' => 'Página',
                'class' => 'btn btn-default',
                'title' => 'Mostrar Primeira página'
            ],
        ];

    public $prepareExportContainer = [];

    /**
     * @var string key to identify showing all data
     */
    protected $_prepareExportKey;


    /**
     * @inheritdoc
     */
    public function init()
    {
    	$this->_prepareExportKey = '_pre' . hash('crc32', $this->options['id']);
        parent::init();
        $this->_isShowAll = ArrayHelper::getValue($_GET, $this->_prepareExportKey, 'page') === 'all';
        if ($this->_isShowAll) 
        {
            $this->dataProvider->pagination = false;
            $this->prepareExport = false;
        }
        else
        {
        	//$this->export = false;
        }
    }

    /**
     * Renders the toggle data button
     *
     * @return string
     */
    public function renderPrepareExport()
    {
        if (!$this->prepareExport) {
            return '';
        }
        
        $tag = $this->_isShowAll ? 'page' : 'all';
        $label = ArrayHelper::remove($this->prepareExportOptions[$tag], 'label', '');
        $url = Url::current([$this->_prepareExportKey => $tag]);
        Html::addCssClass($this->prepareExportContainer, 'btn-group');//print_r($label);die();
        return Html::tag('div', Html::a("<i class='glyphicon glyphicon-export'></i> Preparar Exportação",
         	$url, $this->prepareExportOptions[$tag]), $this->prepareExportContainer);
    }

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function run()
    {
        //$this->initToggleData();
        $this->initExport();
        $this->initPrepareExport();
        	
        if ($this->export !== false && isset($this->exportConfig[self::PDF])) {
            \kartik\base\Config::checkDependency(
                'mpdf\Pdf',
                'yii2-mpdf',
                "for PDF export functionality. To include PDF export, follow the install steps below. If you do not need PDF export functionality, do not include 'PDF' as a format in the 'export' property. You can otherwise set 'export' to false to disable all export functionality"
            );
        }
        $this->initHeader();
        $this->initBootstrapStyle();
        $this->containerOptions['id'] = $this->options['id'] . '-container';
        Html::addCssClass($this->containerOptions, 'kv-grid-container');
        $this->registerAssets();
        $this->renderPanel();
        $this->initLayout();
        $this->beginPjax();
        parent::run();
        $this->endPjax();
    }


    /**
     * Initialize toggle data button options
     */
    protected function initPrepareExport()
    {
        if (!$this->prepareExport) {
            return;
        }
        $defaultOptions = [
            'all' => [
                'icon' => 'resize-full',
                'label' => 'Preparar Exportacao',
                'class' => 'btn btn-default',
                'title' => 'Preparar Exportacao'
            ],
            'page' => [
                'icon' => 'resize-small',
                'label' => 'Pagina',
                'class' => 'btn btn-default',
                'title' => 'Mostrar Primeira pagina'
            ],
        ];
        if (empty($this->prepareExportOptions['page'])) {
            $this->prepareExportOptions['page'] = $defaultOptions['page'];
        }
        if (empty($this->prepareExportOptions['all'])) {
            $this->prepareExportOptions['all'] = $defaultOptions['all'];
        }

        $tag = $this->_isShowAll ? 'page' : 'all';
        $options = $this->prepareExportOptions[$tag];
        $icon = ArrayHelper::remove($this->prepareExportOptions[$tag], 'icon', 'print');
        $label = !isset($options['label']) ? $defaultOptions[$tag]['label'] : $options['label'];
        $label = 'Preparar Exportação';
        if (!empty($icon)) {
            $label = "<i class='glyphicon glyphicon-{$icon}'></i> " . $label;
        }
        $this->prepareExportOptions[$tag]['label'] = $label;
        if (!isset($this->prepareExportOptions[$tag]['title'])) {
            $this->prepareExportOptions[$tag]['title'] = $defaultOptions[$tag]['title'];
        }
        $this->prepareExportOptions[$tag]['data-pjax'] = $this->pjax ? "true" : false;
    }

    /**
     * Initalize grid layout
     */
    protected function initLayout()
    {
        Html::addCssClass($this->filterRowOptions, 'skip-export');
        if ($this->resizableColumns && $this->persistResize) {
            $key = empty($this->resizeStorageKey) ? Yii::$app->user->id : $this->resizeStorageKey;
            $gridId = empty($this->options['id']) ? $this->getId() : $this->options['id'];
            $this->containerOptions['data-resizable-columns-id'] = (empty($key) ? "kv-{$gridId}" : "kv-{$key}-{$gridId}");
        }
        $export = ($this->_isShowAll)?$this->renderExport():'';
        $toggleData = $this->renderToggleData();
        $prepareExport = $this->renderPrepareExport();
        $toolbar = strtr(
            $this->renderToolbar(),
            [
                '{export}' => $export,
                //'{toggleData}' => $toggleData,
                '{prepareExport}' => $prepareExport,
            ]
        );
        $replace = ['{toolbar}' => $toolbar];
        if (($this->_isShowAll) && (strpos($this->layout, '{export}') > 0)) {
            $replace['{export}'] = $export;
        }
        /*if (strpos($this->layout, '{toggleData}') > 0) {
            $replace['{toggleData}'] = $toggleData;
        }*/
        if (strpos($this->layout, '{prepareExport}') > 0) {
            $replace['{prepareExport}'] = $prepareExport;
        }
        $this->layout = strtr($this->layout, $replace);
        $this->layout = str_replace('{items}', Html::tag('div', '{items}', $this->containerOptions), $this->layout);
        if (is_array($this->replaceTags) && !empty($this->replaceTags)) {
            foreach ($this->replaceTags as $key => $value) {
                if ($value instanceof \Closure) {
                    $value = call_user_func($value, $this);
                }
                $this->layout = str_replace($key, $value, $this->layout);
            }
        }
    }
}
