<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
  <div class="login-logo">
      <h1 style="color:white;">PSA Matriculas</h1>
      
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Entre para iniciar sua sessão</p>

    <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'class'=>'form-control'
            ]); ?>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'username')->label(false)->textInput(['autofocus' => true]) ?>
                <span style="font-size: 30px;width: 70px;" class="ion ion-person form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?= $form->field($model, 'passwordHash')->label(false)->passwordInput() ?>
                <span style="font-size: 30px;width: 70px;" class="ion ion-locked form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="fog-pwd">                       
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-12 text-center">
                    <?= Html::submitButton('Entrar', ['class' => 'btn btn-info btn-block margin-top-10', 'name' => 'login-button']) ?>
                </div>
                <!-- /.col -->
            </div>
        <?php ActiveForm::end(); ?>


    <div class="margin-top-30 text-center">
    </div>
    </div>
  <!-- /.login-box-body -->
</div>