<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\DisciplinaPreRequisitoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disciplina Pre Requisitos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="disciplina-pre-requisito-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Disciplina Pre Requisito', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'disciplina_id',
            'pre_disciplina_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
