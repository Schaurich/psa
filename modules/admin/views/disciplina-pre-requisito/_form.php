<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\DisciplinaPreRequisito */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disciplina-pre-requisito-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'disciplina_id')->textInput() ?>

    <?= $form->field($model, 'pre_disciplina_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
