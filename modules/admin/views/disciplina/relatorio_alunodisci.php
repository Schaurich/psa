<?php

use yii\helpers\Html;
use app\models\Helper;
use app\modules\admin\models\Disciplina;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\modules\admin\models\DisciplinaAlunoTurma;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

$this->title = 'Lead Lista';
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Disciplina
            <small>Lista</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">
                        <?php
                            // View file rendering the widget
                            //http://demos.krajee.com/grid-demo
                            echo GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'pjax' => false, // pjax is set to always true for this demo
                                'bordered' => false,
                                'striped' => false,
                                'condensed' => false,
                                'responsive' => true,
                                'hover' => true,
                                'persistResize' => true,
                                'columns' => [
                                    [
                                        'label'=>'Disciplina',
                                        'attribute'=>'id_disciplina',
                                        'vAlign'=>'middle',
                                        'format'=>'raw',
                                        'value'=>function ($model) {
                                            return $model->disciplina->nome;
                                        },
                                        'filterType'=>GridView::FILTER_SELECT2,
                                        'filter' => ArrayHelper::map(Disciplina::find()->all(),'id','nome'),
                                        'filterInputOptions' => ['placeholder' => 'Disciplina'],
                                        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true]],
                                    ],
                                    [
                                        'label'=>'Aluno',
                                        'attribute'=>'user.id',
                                        'vAlign'=>'middle',
                                        'format'=>'raw',
                                        'value'=>function ($model) {
                                            return $model->user->id;
                                        },
                                        'filterType'=>GridView::FILTER_SELECT2,
                                        'filter' => ArrayHelper::map(Disciplina::find()->all(),'id','nome'),
                                        'filterInputOptions' => ['placeholder' => 'Disciplina'],
                                        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true]],
                                    ],
                                    [
                                        'label'=>'Turma',
                                        'attribute'=>'turma.id',
                                        'vAlign'=>'middle',
                                        'format'=>'raw',
                                        'value'=>function ($model) {
                                            return $model->turma->id;
                                        },
                                        'filterType'=>GridView::FILTER_SELECT2,
                                        'filter' => ArrayHelper::map(Disciplina::find()->all(),'id','nome'),
                                        'filterInputOptions' => ['placeholder' => 'Disciplina'],
                                        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true]],
                                    ],
                                ],
                            ]);
                        ?>

                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

    

