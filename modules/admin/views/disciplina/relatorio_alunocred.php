<?php

use yii\helpers\Html;
use app\models\Helper;
use app\modules\admin\models\User;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\modules\admin\models\DisciplinaAlunoTurma;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

$this->title = 'Lead Lista';
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Disciplina
            <small>Lista</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">
                        <?php
                            $resultado = 0;
                            foreach($arr_aluno as $key => $value): ?>
                        <div class="col-md-6">
                            <?php
                                $disciplinaTurma = User::find()->where(['id'=>$key])->one();
                               
                                echo "<b>Matricula:</b> {$disciplinaTurma->id}";
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                                
                                echo "<b>Resultado:</b> {$value}";
                            ?>
                        </div>
                        <?php endforeach; ?>
                        <div class="row" style="margin-top: 10%;">
                            <div class="col-md-4" ></div>
                            <div class="col-md-4">
                                <?= "<b>Total: </b> {$total}"?>
                            </div>
                            <div class="col-md-4" ></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

    

