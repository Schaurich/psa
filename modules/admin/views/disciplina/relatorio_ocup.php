<?php

use yii\helpers\Html;
use app\models\Helper;
use app\modules\admin\models\DisciplinaTurma;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\modules\admin\models\DisciplinaAlunoTurma;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

$this->title = 'Relatório %';
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Relatório
            <small>%</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">
                        
                        <?php
                            $resultado = 0;
                            foreach($arr_disci as $key => $value): ?>
                        <div class="col-md-6">
                            <?php
                                $disciplinaTurma = DisciplinaTurma::find()->where(['disciplina_id'=>$key])->one();
                               
                                echo "<b>Disciplina:</b> {$disciplinaTurma->disciplina->nome}";
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                                $disciplinaTurma = DisciplinaTurma::find()->where(['disciplina_id'=>$key])->one();
                                $resultado = number_format((($value/$disciplinaTurma->vagas)*100),2,",",".");
                                $resultado = $resultado." %";
                                echo "<b>Resultado:</b> {$resultado}";
                            ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

    

