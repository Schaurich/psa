<?php

use yii\helpers\Html;
use app\models\Helper;
use app\modules\admin\models\Disciplina;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\modules\admin\models\DisciplinaAlunoTurma;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

$this->title = 'Matricula';
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Disciplina
            <small>Lista</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        <?php
                            echo Html::a("Comprovante de Matricula", ['disciplina-aluno-turma/comprovante_matricula','id'=>\Yii::$app->user->identity->id], ['class' => 'btn btn-success','target'=>'_blank']);
                        ?>
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">
                        <?php
                            // View file rendering the widget
                            //http://demos.krajee.com/grid-demo
                            echo GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'rowOptions'=>function($model){
                                    $user = \Yii::$app->user->identity;
                                    $historico = DisciplinaAlunoTurma::find()->where(['id_user'=>$user->id,'id_disciplina'=>$model->id])->orderby(['id'=>SORT_DESC])->one();
                                    if($historico){
                                        if($historico->situacao=='APR')
                                        {    
                                            return ['class' => 'success'];
                                        }
                                        if($historico->situacao=='MAT')
                                        {  
                                            return ['class' => 'warning'];
                                        }
                                    }
                                },
                                'pjax' => false, // pjax is set to always true for this demo
                                'bordered' => false,
                                'striped' => false,
                                'condensed' => false,
                                'responsive' => true,
                                'hover' => true,
                                'persistResize' => true,
                                'columns' => [
                                    [
                                        'label'=>'Disciplina',
                                        'attribute'=>'id',
                                        'vAlign'=>'middle',
                                        'format'=>'raw',
                                        'value'=>function ($model) {
                                            return $model->nome;
                                        },
                                        'filterType'=>GridView::FILTER_SELECT2,
                                        'filter' => ArrayHelper::map(Disciplina::find()->all(),'id','nome'),
                                        'filterInputOptions' => ['placeholder' => 'Disciplina'],
                                        'filterWidgetOptions'=>['pluginOptions'=>['allowClear'=>true]],
                                    ],
                                    'semestre',
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'header'=>'Ações',
                                        'headerOptions' => ['class'=>'cor-azul','width' => '80'],
                                        'template' => '{editar}{delete}',
                                        'buttons' => [
                                            'editar' => function ($url,$model) 
                                            {
                                                $user = \Yii::$app->user->identity;
                                                $historico = DisciplinaAlunoTurma::find()->where(['id_user'=>$user->id,'id_disciplina'=>$model->id])->orderby(['id'=>SORT_DESC])->one();
                                                $matriculados = count(DisciplinaAlunoTurma::find()->where(['id_disciplina'=>$model->id,'id_turma'=>'168','situacao'=>'MAT'])->orderby(['id'=>SORT_DESC])->all());
                                                $vagas = 3-$matriculados;
                                                
                                                if(!$historico || $historico->situacao == 'DEL'){
                                                    return Html::a("<i class='glyphicon glyphicon-check' title='Vagas: {$vagas}' data-toggle='tooltip'></i>", ['disciplina-aluno-turma/create','id_disciplina'=>$model->id,'id_turma'=>'168','situacao'=>'MAT']).' ';
                                                }
                                                
                                            },
                                            'delete' => function ($url,$model) 
                                            {
                                                $user = \Yii::$app->user->identity;
                                                $matricula = DisciplinaAlunoTurma::find()->where(['id_user'=>$user->id,'id_disciplina'=>$model->id,'id_turma'=>'168','situacao'=>'MAT'])->orderby(['id'=>SORT_DESC])->one();
                                                
                                                if($matricula){
                                                    return Html::a("<i class='glyphicon glyphicon-trash' title='Exlcuir Matricula' data-toggle='tooltip'></i>", ['disciplina-aluno-turma/update','id'=>$matricula->id],['method'=>'post']).' ';
                                                }
                                                
                                            },
                                            
                                          ],
                                    ]  
                                    
                                    
                                ],
                            ]);
                        ?>

                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

    

