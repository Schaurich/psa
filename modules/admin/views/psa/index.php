<?php

use yii\helpers\Html;
use app\models\Helper;
$this->title = 'Início';
?>
<style>
    .tilt {
        transform: rotate(3deg);
        -moz-transform: rotate(3deg);
        -webkit-transform: rotate(3deg);
    }

    .column {
        float: left;
        padding-bottom: 100px;
    }
    .portlet {
        margin: 0 1em 1em 0;
        padding: 0.3em;
    }
    .portlet-header {
        padding: 0.2em 0.3em;
        margin-bottom: 0.5em;
        position: relative;
    }
    .portlet-toggle {
        position: absolute;
        top: 50%;
        right: 0;
        margin-top: -8px;
    }
    .portlet-content {
        padding: 0.4em;
    }
    .portlet-placeholder {
        border: 1px dotted black;
        margin: 0 1em 1em 0;
        height: 50px;
    }
</style>
<input type="hidden" id="pageurl" value="<?= Yii::$app->urlManager->createUrl(['admin/lead/updateindex']) ?>">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Início
            <small>PSA Matriculas</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        <?php
                            if(Yii::$app->user->identity->id != 1){
                                echo Html::a('Matricula',['disciplina/index'],['class'=>'btn btn-md btn-success']);
                            }
                            else{
                                echo Html::a('Relatorio %',['disciplina/relatorio_ocup'],['class'=>'btn btn-md btn-success','target'=>'_blank']);
                                echo ' '.Html::a('Aluno Disciplina',['disciplina/relatorio_alunodisci'],['class'=>'btn btn-md btn-success','target'=>'_blank']);
                                echo ' '.Html::a('Aluno Crédito Total',['disciplina/relatorio_alunocred'],['class'=>'btn btn-md btn-success','target'=>'_blank']);
                            }
                        ?>
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12 col-md-3">
                    
                    </div>
                </div>
            </div>
        </section>
</div>