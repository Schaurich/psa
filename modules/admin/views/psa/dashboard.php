<?php

use yii\helpers\Html;
use app\models\Helper;

?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Criados', 'Oportunidades', 'Leads','Compromissos'],
          ['jan/18', 1000,100,100],
          ['fev/18', 900, 1000,100],
          ['mar/18', 800, 1000,100],
          ['abr/18', 600, 1000,100],
          ['mai/18', 1000, 1000,100],
          ['jun/18', 1200, 1000,100],
          ['jul/18', 800, 1000,100],
          ['ago/18', 100, 1000,100],
          ['set/18', 600, 1000,100],
          ['out/18', 500, 1000,100],
          ['nov/18', 400, 1000,100],
          ['dez/18', 300, 1000,100]
        ]);

        var options = {
          chart: {
            title: 'Criados no ano',
            subtitle: '',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('chart_criados_ano'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }
    </script>
<input type="hidden" id="pageurl" value="<?= Yii::$app->urlManager->createUrl(['admin/lead/updateindex']) ?>">
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Dashboard
            <small>Gestão de Contatos</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li c   lass="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12 col-md-3">
                        <div id="chart_criados_ano" style="width: 600px; height: 400px;"></div>
                    </div>
                </div>
            </div>
        </section>
</div>

