<?php 
    use app\modules\admin\models\User;
    use yii\bootstrap\Html;
    $model = User::findOne(Yii::$app->user->identity->id);
    $nome = explode(' ',$model->username);
?>


<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="user-profile treeview">
                <a href="">
                    <img src="<?= '../../minimalliteadmin/images/user5-128x128.jpg'?>" alt="user">
                    <span style="text-transform: capitalize;"><?=  (isset($nome[0]))?$nome[0]:"";?></span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-right pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <?php Html::a('<i class="fa fa-user mr-5"></i>Minha conta',['user/view','id'=>$model->id])?>
                    </li>
                    <li>
                        <?= Html::a('<i class="fa fa-circle-thin"></i>Inicio',['psa/index'])?>
                    </li>
                    <?php if($model->id > 10):?>
                     <li>
                        <?= Html::a('<i class="fa fa-circle-thin"></i>Matricular',['disciplina/index'])?>
                    </li>
                    <li>
                        <?= Html::a('<i class="fa fa-circle-thin"></i>Comprovante Matricula',['disciplina-aluno-turma/comprovante_matricula','id'=>\Yii::$app->user->identity->id], ['target'=>'_blank'])?>
                    </li>
                     <?php endif; ?>
                     <?php if($model->id < 10):?>
                     <li>
                        <?= Html::a('<i class="fa fa-circle-thin"></i>Relatório Ocupação',['disciplina/relatorio_ocup'])?>
                    </li>
                     <li>
                        <?= Html::a('<i class="fa fa-circle-thin"></i>Relatório Credito',['disciplina/relatorio_alunocred'])?>
                    </li>
                     <li>
                        <?= Html::a('<i class="fa fa-circle-thin"></i>Relatório Disciplina',['disciplina/relatorio_alunodisci'])?>
                    </li>
                        <?php endif; ?>          
                    <li><?= Html::a('<i class="fa fa-power-off mr-5"></i> Sair', ['default/logout']) ?></li>
                </ul>
            </li>
            
        </ul>
    </section>
</aside>


                 