<?php

use yii\helpers\Html;
use app\models\Helper;
$this->title = "Configurações Gerais";
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Configurações
            <small>Gerais</small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>

    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        <?= Html::a('Usuários', ['user/index'], ['class' => 'btn btn-sm btn-dark','style'=>'margin: 3px 0;']) ?>
                        <?= Html::a('Carteira', ['carteira/index'], ['class' => 'btn btn-sm btn-danger','style'=>'margin: 3px 0;']) ?>
                        <?php Html::a('Compromisso Grupo', ['compromisso-grupo/index'], ['class' => 'btn btn-sm btn-warning','style'=>'margin: 3px 0;']) ?>
                        <?php Html::a('Compromisso Status', ['compromisso-status/index'], ['class' => 'btn btn-sm btn-warning','style'=>'margin: 3px 0;']) ?>
                        <?php Html::a('Compromisso Tipo', ['compromisso-tipo/index'], ['class' => 'btn btn-sm btn-warning','style'=>'margin: 3px 0;']) ?>
                        <?php Html::a('Lead Situação', ['lead-situacao/index'], ['class' => 'btn btn-sm btn-success','style'=>'margin: 3px 0;']) ?>
                        <?php Html::a('Lead Status', ['lead-status/index'], ['class' => 'btn btn-sm btn-success','style'=>'margin: 3px 0;']) ?>
                        <?php Html::a('Oportunidade Status', ['oportunidade-status/index'], ['class' => 'btn btn-sm btn-info','style'=>'margin: 3px 0;']) ?>
                        <?php Html::a('Oportunidade Tipo', ['oportunidade-tipo/index'], ['class' => 'btn btn-sm btn-info','style'=>'margin: 3px 0;']) ?>
                        <?= Html::a('Produto', ['produto/index'], ['class' => 'btn btn-sm btn-secondary','style'=>'margin: 3px 0;']) ?>
                        
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
<!--                <div class="box-body">
                    
                </div>-->
            </div>
        </section>
</div>

