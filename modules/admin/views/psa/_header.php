<header class="main-header">
    <a href="" class="logo">
        <b class="logo-mini">
<!--            <span class="light-logo"><img src="../../images/gsn_logo.png" alt="logo"></span>
            <span class="dark-logo"><img src="../../images/gsn_logo.png" alt="logo"></span>-->
        </b>
        <span class="logo-lg">
<!--            <img src="../../minimalliteadmin/images/logo-light-text.png" alt="logo" class="light-logo">
            <img src="../../minimalliteadmin/images/logo-dark-text.png" alt="logo" class="dark-logo">-->
        </span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
<!--                <li class="search-box">
                    <a class="nav-link hidden-sm-down" href="javascript:void(0)"><i class="mdi mdi-magnify"></i></a>
                    <form class="app-search" style="display: none;">
                        <input type="text" class="form-control" placeholder="Pesquisar"> <a class="srh-btn"><i class="ti-close"></i></a>
                    </form>
                </li>		-->
                <li style="list-style-type: disc;color:transparent">
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-cog fa-spin"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>