<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\DisciplinaTurma */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disciplina-turma-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'turma_id')->textInput() ?>

    <?= $form->field($model, 'disciplina_id')->textInput() ?>

    <?= $form->field($model, 'turno')->textInput() ?>

    <?= $form->field($model, 'horario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'user_create')->textInput() ?>

    <?= $form->field($model, 'data_create')->textInput() ?>

    <?= $form->field($model, 'user_update')->textInput() ?>

    <?= $form->field($model, 'data_update')->textInput() ?>

    <?= $form->field($model, 'vagas')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
