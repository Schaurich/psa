<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\User */

$this->title = ($model->isNewRecord)? "Usuário Criar" : "Usuário Atualizar";
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Usuário
            <small><?= ($model->isNewRecord)? "Criar" : "Atualizar" ?></small>
        </h1>
        <ol class="breadcrumb">
            <!--<li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i> Início</a></li>-->
        </ol>
    </section>
    <?php $form = ActiveForm::begin(); ?>
    <section class="content">
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <div class="form-group">
                        <?=
                        Html::a("Voltar", ['user/view', 'id' => ($model->id)?$model->id:$_GET['id']], ['class' => 'btn btn-light']);
                        ?>
                        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success pull-right']) ?>
                    </div>
                    <div class="box-tools pull-right">
                        <!--                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                                        <i class="fa fa-minus"></i></button>
                                                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                        <i class="fa fa-times"></i></button>-->
                    </div>
                </div>
                <div class="box-body">
                    <div class="column col-sm-12">

                        <h1><?= ($model->isNewRecord)? "Criar" : "Atualize" ?></h1>


                        <div class="col-sm-6 b-r">
                        <?= $form->field($model, 'nome')->textInput(['maxlength' => true]) ?>
                        <?php
                        if ($model->isNewRecord) {
                            echo $form->field($model, 'username')->label('login')->textInput(['maxlength' => true]);
                            echo $form->field($model, 'passwordHash')->label('Senha')->passwordInput(['maxlength' => true]);
                        }
                        ?>
                        <?=
                        $form->field($model, 'cpf')->widget(MaskedInput::classname(), [
                            'mask' => ['999.999.999-99']
                        ]);
                        ?>
                        <?=
                        $form->field($model, 'celular')->widget(MaskedInput::classname(), [
                            'mask' => ['(99)99999-9999']
                        ]);
                        ?>
                        </div>
                        <div class="col-sm-6 b-r">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                        <?=
                        $form->field($model, 'arquivo')->label('Foto')->widget(FileInput::classname(), [
                            'options' => ['accept' => 'image/*'],
                        ]);
                        ?>
                        </div>
<?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </section>
    </section>
</div>

