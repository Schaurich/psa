<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Registro';
?>

<div class="register-box">
    
    <div class="register-logo">
        <?= Html::a('<b>Gerencie seu</b> Negócio',[''],['style'=>'font-size: 24px;']) ?>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Cadastre-se</p>
        <?php
        $form = ActiveForm::begin([
                    'id' => 'login-form',
//                'layout' => 'horizontal',
//            'fieldConfig' => [
////                'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
////                'labelOptions' => ['class' => 'col-lg-12 control-label'],
//            ],
                    'class' => 'form-element'
        ]);
        ?>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'nome')->label(false)->textInput(['class' => 'form-control', 'autofocus' => true, 'placeholder' => 'Nome']) ?>
            <span style="font-size: 30px;" class="ion ion-person form-control-feedback "></span>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'username')->label(false)->textInput(['class' => 'form-control', 'autofocus' => true, 'placeholder' => 'Username']) ?>
            <span style="font-size: 30px;" class="ion ion-person form-control-feedback "></span>
        </div>
        <div class="form-group has-feedback">
            <?= $form->field($model, 'email')->label(false)->textInput(['class' => 'form-control', 'placeholder' => 'Email']) ?>
            <span style="font-size: 30px;" class="ion ion-email form-control-feedback "></span>
        </div>
        <div class="form-group has-feedback">
<?= $form->field($model, 'passwordHash')->label(false)->passwordInput(['class' => 'form-control', 'placeholder' => 'Password']) ?>
            <span style="font-size: 30px;" class="ion ion-locked form-control-feedback "></span>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="checkbox">
                    <input type="checkbox" id="basic_checkbox_1" name="User[aceito]" >
                    <label for="basic_checkbox_1">Eu aceito os <a href="#" data-toggle="modal" data-target="#myModal"><b>Termos</b></a></label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-12 text-center">
                <div class="mx-auto g-recaptcha" data-sitekey="6Lfcm3QUAAAAAM1W8mA6XHVHeXbfHTBENzj7YK3c"></div>
                <button type="submit" class="btn btn-info btn-block margin-top-10">Cadastrar</button>
            </div>
            <!-- /.col -->
        </div>
<?php ActiveForm::end(); ?>

        <!--        <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
                </div>-->
        <!-- /.social-auth-links -->

        <div class="margin-top-20 text-center">
            <p>Já tem uma conta? <?= Html::a('Entrar',['default/index'],['class'=>'text-info m-l-5']) ?></p>
        </div>

    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Termos</h4>
            </div>
            <div class="modal-body">
                <p>Diversos termos</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js?hl=pt-BR'></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>