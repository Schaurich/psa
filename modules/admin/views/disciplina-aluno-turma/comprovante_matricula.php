<?php

use app\modules\admin\models\DisciplinaTurma;
use app\models\Helper;

$this->title = 'Comprovante de Matricula';
$aluno = \Yii::$app->user->identity;

?>
<style>
    .wrapper{
        background-color: white !important;
        min-height: 300px !important;
    }
    
</style>
<div style="margin: 0 10%;">
    <div class="col-md-1">

    </div>
    <div class="col-md-10" style="border: 1px solid #000;">
        <div class="col-md-12">
            <p class="text-center" style="font-size:25px; font-weight: bold">Aluno: <?= $aluno->id ?></p>
        </div>
        <?php foreach($models as $model): ?>
        <div class="col-md-6" style="font-size:14px;">
            <?= '<b>Disciplina: </b>'.$model->disciplina->nome ?>
        </div>
        <div class="col-md-3" style="font-size:14px;">
            <?= '<b>Cod Cred: </b>'.$model->disciplina->cod_cred ?>
        </div>
        <div class="col-md-3" style="font-size:14px;">
            <?php 
            
                $disciplina_turma = DisciplinaTurma::find()->where(['disciplina_id'=>$model->disciplina->id,'turma_id'=>$model->turma->id])->one(); 
                $texto = '<b>Horário: </b>'.$disciplina_turma->horario;
                echo Helper::TextSameLine($texto);
            ?>
        </div>
        <?php        endforeach;?>
    </div>
    <div class="col-md-1">

    </div>
</div>
