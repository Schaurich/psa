<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\DisciplinaAlunoTurmaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disciplina-aluno-turma-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_disciplina') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'id_turma') ?>

    <?= $form->field($model, 'situacao') ?>

    <?php // echo $form->field($model, 'data_create') ?>

    <?php // echo $form->field($model, 'user_create') ?>

    <?php // echo $form->field($model, 'data_update') ?>

    <?php // echo $form->field($model, 'user_update') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
