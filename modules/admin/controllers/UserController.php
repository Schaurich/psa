<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\User;
use app\modules\admin\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use app\modules\admin\models\Lead;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['delete', 'index','view','update','create'],
                'rules' => [
                    [
                        'actions' => ['delete', 'index','view','update','create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['register', 'recovery'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

   

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $this->layout = '@app/views/layouts/dashboard.php';
        $user = \Yii::$app->user->identity;
        $leads = count(Lead::find()
                ->select('id')
                ->distinct('id')
                ->where(['status'=>10,'Carteira_id'=>$user->carteira_id])
                ->all());
        $usuarios = count(User::find()->where(['status'=>10,'user_create'=>Yii::$app->user->identity->id])->all());
        
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'usuarios'=>$usuarios,
                    'leads'=>$leads
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister() {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->passwordHash);
            $model->generatePasswordResetToken();
            $model->generateAuthKey();
            $model->data_create = date('Y-m-d H:i:s');
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Registro realizado com sucesso.');
                return $this->redirect(['default/index']);
            } else {
                Yii::$app->session->setFlash('error', 'Erro no registro.');
                return $this->redirect(['default/index']);
            }
        } else {

            return $this->render('register', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $status = '';
        if($model->status)
        {
           $model->status = 0;
           $status = 'excluido';
        }
        else
        {
           $model->status = 10; 
           $status = 'reativado';
        }
        
        if (!$model->save()) {
            ddd($model->getErrors());
        }
        if($model->id == \Yii::$app->user->identity->id)
        {
            Yii::$app->session->setFlash('success', "Conta excluida com sucesso.");
            Yii::$app->user->logout();
            return $this->redirect(['default/index']);
        }
        else
        {
            Yii::$app->session->setFlash('success', "Usuário {$status} com sucesso.");
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRecovery($email = null) {

        $model = new User;

        if (Yii::$app->request->post()) {

            $post = Yii::$app->request->post()['User'];

            $model = User::findByEmail($post['email']);
            if (!$model) {
                $model = new User;
                Yii::$app->session->setFlash('error', 'Usuário não encontrado.');
                return $this->render('recovery', [
                            'model' => $model,
                ]);
            }

            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $pass = []; //remember to declare $pass as an array
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < 8; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }

            $model->setPassword(implode($pass));
            if (!$model->save()) {
                ddd($model->getErrors());
            }

            Yii::$app->mailer->compose()
                    ->setFrom('servicos.ti@grupostudio.com.br')
                    ->setTo($model->email)
                    ->setSubject('Sua senha nova é: ' . implode($pass))
                    ->send();
            Yii::$app->session->setFlash('success', 'Senha enviada para seu e-mail.');
            return $this->redirect(['default/index']);
        }

        return $this->render('recovery', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $this->layout = '@app/views/layouts/dashboard.php';
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            
            $model->data_update = date('Y-m-d H:i:s');
            $data = date('d-m-o_H-i-s');
            if (!file_exists('images/foto_usuario')) {
                $path = 'images/foto_usuario';
                FileHelper::createDirectory($path);
            } else {
                $path = 'images/foto_usuario';
            }
            
            $arquivo = UploadedFile::getInstance($model, 'arquivo');
            if ($arquivo) {
                $valida_nome = explode('.', $arquivo->name);

                $arquivo->name = $data . '.' . end($valida_nome);
                $model->foto = $arquivo->name;

                if ($arquivo->saveAs($path . '/' . $arquivo->name)) {
                    if ($model->save()) {
                        Yii::$app->session->setFlash('success', 'Atualizado com sucesso.');
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'Houve um problema ao salvar.');
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Atualizado com sucesso.');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }
    
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCreate($id) {
        $this->layout = '@app/views/layouts/dashboard.php';
        $model = new User;
        
        if ($model->load(Yii::$app->request->post())) {
            
            $model->setPassword($model->passwordHash);
            $model->generatePasswordResetToken();
            $model->generateAuthKey();
            $model->data_update = date('Y-m-d H:i:s');
            $model->data_create = date('Y-m-d H:i:s');
            $model->user_create = \Yii::$app->user->identity->id;
            
            $data = date('d-m-o_H-i-s');
            if (!file_exists('images/foto_usuario')) {
                $path = 'images/foto_usuario';
                FileHelper::createDirectory($path);
            } else {
                $path = 'images/foto_usuario';
            }
            
            $arquivo = UploadedFile::getInstance($model, 'arquivo');
            if ($arquivo) {
                $valida_nome = explode('.', $arquivo->name);

                $arquivo->name = $data . '.' . end($valida_nome);
                $model->foto = $arquivo->name;

                if ($arquivo->saveAs($path . '/' . $arquivo->name)) {
                    if ($model->save()) {
                        Yii::$app->session->setFlash('success', 'Criado com sucesso.');
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } else {
                    Yii::$app->session->setFlash('error', 'Houve um problema na criação.');
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Criado com sucesso.');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }
     /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        $this->layout = '@app/views/layouts/dashboard.php';
        $model = \Yii::$app->user->identity;
        $usuarios = count(User::find()->where(['user_create'=>Yii::$app->user->identity->id])->all());
        
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'usuarios'=>$usuarios,
                    'model'=>$model
        ]);
    }

}
