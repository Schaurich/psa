<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\DisciplinaAlunoTurma;
use app\modules\admin\models\DisciplinaPreRequisito;
use app\modules\admin\models\DisciplinaAlunoTurmaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DisciplinaAlunoTurmaController implements the CRUD actions for DisciplinaAlunoTurma model.
 */
class DisciplinaAlunoTurmaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DisciplinaAlunoTurma models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DisciplinaAlunoTurmaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DisciplinaAlunoTurma model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DisciplinaAlunoTurma model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DisciplinaAlunoTurma();
        $user = \Yii::$app->user->identity;
        
        if (Yii::$app->request->get()) 
        {
            $get = Yii::$app->request->get();
            $matricula_excluida = DisciplinaAlunoTurma::find()->where(['id_user'=>$user->id,'id_disciplina'=>$get['id_disciplina'],'id_turma'=>$get['id_turma'],'situacao'=>'DEL'])->orderby(['id'=>SORT_DESC])->one();
            
            $pre_requisitos = DisciplinaPreRequisito::find()->where(['disciplina_id'=>$get['id_disciplina']])->all();
            
            foreach($pre_requisitos as $pre_requisito)
            {
                
                $aprovado = DisciplinaAlunoTurma::find()->where([
                    'id_user'=>$user->id,
                    'id_disciplina'=>$pre_requisito->pre_disciplina_id,
                    'situacao'=>'APR'
                    
                ])->orderby(['id'=>SORT_DESC])->all();
                
                if(!$aprovado)
                {
                    Yii::$app->session->setFlash('error', 'Você não concluiu os pré requisitos.');
                    return $this->redirect(['disciplina/index']);
                }
                
            }
            $count = count(DisciplinaAlunoTurma::find()->where(['id_disciplina'=>$get['id_disciplina'],'id_turma'=>'168','situacao'=>'MAT'])->orderby(['id'=>SORT_DESC])->all());
            if($count<3)
            {
                if($matricula_excluida)
                {
                    $matricula_excluida->situacao = 'MAT';
                    if(!$matricula_excluida->save())
                    {
                        ddd($matricula_excluida->getErrors());
                    }
                
                    Yii::$app->session->setFlash('success', 'Matriculado com sucesso.');
                    return $this->redirect(['disciplina/index']);
                }
                $model->id_disciplina = $get['id_disciplina'];
                $model->id_turma = $get['id_turma'];
                $model->situacao = $get['situacao'];
                $model->id_user = $user->id;
                
                if(!$model->save())
                {
                    ddd($model->getErrors());
                }
                
                Yii::$app->session->setFlash('success', 'Matriculado com sucesso.');
            }
            else{
                Yii::$app->session->setFlash('error', 'Limite atingido.');
            }
            return $this->redirect(['disciplina/index']);
        }
    }

    /**
     * Updates an existing DisciplinaAlunoTurma model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->situacao = 'DEL';

        if (!$model->save()) {
           ddd($model->getErrors()); 
        }
        Yii::$app->session->setFlash('error', 'Matricula excluida com sucesso.');
        return $this->redirect(['disciplina/index']);
    }
    
     /**
     * Updates an existing DisciplinaAlunoTurma model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionComprovante_matricula($id)
    {
        $models = DisciplinaAlunoTurma::find()->where(['id_user'=>$id,'situacao'=>'MAT'])->all();
        $this->layout = '@app/views/layouts/relatorio.php';
        
        return $this->render('comprovante_matricula',[
                'models'=>$models
            ]);
    }

    /**
     * Deletes an existing DisciplinaAlunoTurma model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DisciplinaAlunoTurma model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DisciplinaAlunoTurma the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DisciplinaAlunoTurma::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
