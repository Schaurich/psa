<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\admin\models\LoginForm;
use app\modules\admin\models\Disciplina;
use app\modules\admin\models\DisciplinaSearch;

/**
 * Default controller for the `admin` module
 */
class PsaController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $this->layout = '@app/views/layouts/dashboard.php';
        $user = \Yii::$app->user->identity;
        
        $searchModel = new DisciplinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        
        return $this->render('index',[
            'model'=>$user,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            
        ]);
    }
    
    public function actionConfiguracao() {
        $this->layout = '@app/views/layouts/dashboard.php';
        
        return $this->render('configuracao');
    }
    
    public function actionDashboard() {
        $this->layout = '@app/views/layouts/dashboard.php';
        
        return $this->render('dashboard');
    }


}
