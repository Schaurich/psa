<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Disciplina;
use app\modules\admin\models\DisciplinaSearch;
use app\modules\admin\models\DisciplinaAlunoTurmaSearch;
use app\modules\admin\models\DisciplinaAlunoTurma;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DisciplinaController implements the CRUD actions for Disciplina model.
 */
class DisciplinaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Disciplina models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/dashboard.php';
        $searchModel = new DisciplinaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionRelatorio_ocup()
    {
        $this->layout = '@app/views/layouts/dashboard.php';
        $disciplinas = DisciplinaAlunoTurma::find()->where(['situacao'=>'MAT'])->all();
        $arr_disci = [];
        foreach($disciplinas as $value)
        {
            if(!array_key_exists($value->id_disciplina, $arr_disci)){
                $arr_disci[$value->id_disciplina] = 1;
            }
            else{
                $arr_disci[$value->id_disciplina] += 1;
            }
            
        }

        return $this->render('relatorio_ocup',['arr_disci'=>$arr_disci]);
    }
    public function actionRelatorio_alunodisci()
    {
        $this->layout = '@app/views/layouts/dashboard.php';
        $searchModel = new DisciplinaAlunoTurmaSearch();
        $dataProvider = $searchModel->search_mat(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        return $this->render('relatorio_alunodisci', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionRelatorio_alunocred()
    {
        $this->layout = '@app/views/layouts/dashboard.php';
        $disciplinas = DisciplinaAlunoTurma::find()->where(['situacao'=>'MAT'])->all();
        $arr_aluno = [];
        $total = 0;
        foreach($disciplinas as $value)
        {
            if(!array_key_exists($value->id_user, $arr_aluno)){
                $arr_aluno[$value->id_user] = $value->disciplina->credito;
            }
            else{
                $arr_aluno[$value->id_user] += $value->disciplina->credito;
            }
            $total += $value->disciplina->credito;
        }
        
        return $this->render('relatorio_alunocred',['arr_aluno'=>$arr_aluno,'total'=>$total]);
        
    }

    /**
     * Displays a single Disciplina model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Disciplina model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Disciplina();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Disciplina model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Disciplina model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Disciplina model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Disciplina the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Disciplina::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
