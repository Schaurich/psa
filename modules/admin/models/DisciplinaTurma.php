<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "disciplina_turma".
 *
 * @property int $id
 * @property int $turma_id
 * @property int $disciplina_id
 * @property int $turno
 * @property string $horario
 * @property int $user_create
 * @property string $data_create
 * @property int $user_update
 * @property string $data_update
 * @property int $vagas
 *
 * @property Disciplina $disciplina
 * @property Turma $turma
 * @property User $userCreate
 * @property User $userUpdate
 */
class DisciplinaTurma extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'disciplina_turma';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['turma_id', 'disciplina_id', 'turno', 'user_create', 'user_update', 'vagas'], 'integer'],
            [['data_create', 'data_update'], 'safe'],
            [['horario'], 'string', 'max' => 45],
            [['disciplina_id'], 'exist', 'skipOnError' => true, 'targetClass' => Disciplina::className(), 'targetAttribute' => ['disciplina_id' => 'id']],
            [['turma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Turma::className(), 'targetAttribute' => ['turma_id' => 'id']],
            [['user_create'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_create' => 'id']],
            [['user_update'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_update' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'turma_id' => 'Turma ID',
            'disciplina_id' => 'Disciplina ID',
            'turno' => 'Turno',
            'horario' => 'Horario',
            'user_create' => 'User Create',
            'data_create' => 'Data Create',
            'user_update' => 'User Update',
            'data_update' => 'Data Update',
            'vagas' => 'Vagas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplina()
    {
        return $this->hasOne(Disciplina::className(), ['id' => 'disciplina_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma()
    {
        return $this->hasOne(Turma::className(), ['id' => 'turma_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_update']);
    }
}
