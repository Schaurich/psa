<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "user_historico".
 *
 * @property int $id
 * @property string $matricula
 * @property string $codcred
 * @property string $status
 * @property string $turma
 */
class UserHistorico extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_historico';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula', 'codcred', 'status', 'turma'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'matricula' => 'Matricula',
            'codcred' => 'Codcred',
            'status' => 'Status',
            'turma' => 'Turma',
        ];
    }
}
