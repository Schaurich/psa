<?php
namespace app\modules\admin\models;
use Yii;
use \yii\db\ActiveRecord;
use \yii\web\IdentityInterface;
use \yii\base\NotSupportedException;
use \yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "{{%users}}".
 *
  * @property int $id
 * @property string $username
 * @property string $email
 * @property string $passwordHash
 * @property string $passwordResetToken
 * @property int $matricula_id
 * @property string $nome
 * @property int $status
 * @property string $data_create
 * @property int $user_create
 * @property string $data_update
 * @property string $authKey
 * @property string $updated_at
 *
 * @property User $userCreate
 * @property User[] $users
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE  = 10;
    public $arquivo;
    public static function tableName()
    {
        return 'user';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['matricula_id', 'status', 'user_create'], 'integer'],
            [['data_create', 'data_update', 'updated_at', 'created_at'], 'safe'],
            [['authKey'], 'string'],
            [['username', 'nome'], 'string', 'max' => 45],
            [['email', 'passwordHash', 'passwordResetToken'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['username'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['matricula_id'], 'exist', 'skipOnError' => true, 'targetClass' => Matricula::className(), 'targetAttribute' => ['carteira_id' => 'id']],
            [['user_create'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_create' => 'id']],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'passwordHash' => 'Password Hash',
            'passwordResetToken' => 'Password Reset Token',
            'matricula_id' => 'Matricula ID',
            'nome' => 'Nome',
            'cpf' => 'Cpf',
            'celular' => 'Celular',
            'status' => 'Status',
            'data_create' => 'Data Criação',
            'user_create' => 'User Create',
            'data_update' => 'Data Atualização',
            'authKey' => 'Auth Key',
            'foto' => 'Foto',
        ];
    }
    public function scenarios()
    {
        $scenarios = parent::scenarios();
//        $scenarios['update'] = ['username', 'email'];
        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }
    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    /**
     * Finds user by email
     *
     * @param  string      $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }
        return static::findOne([
            'passwordResetToken' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->passwordHash);
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->passwordHash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->authKey = Yii::$app->security->generateRandomString();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Yii::$app->security->generateRandomString() . '_' . time();
    }
    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;
    }
    public function requestPasswordResetToken($id)
    {
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
        if (!$user) {
            return false;
        }
        if (!User::isPasswordResetTokenValid($user->passwordResetToken)) {
            $user->generatePasswordResetToken();
        }
        if (!$user->save()) {
            return false;
        }
        return $user->passwordResetToken;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompromissos()
    {
        return $this->hasMany(Compromisso::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOportunidades()
    {
        return $this->hasMany(Oportunidade::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarteira()
    {
        return $this->hasOne(Carteira::className(), ['id' => 'carteira_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_create' => 'id']);
    }
}