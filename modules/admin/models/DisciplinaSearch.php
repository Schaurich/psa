<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\Disciplina;

/**
 * DisciplinaSearch represents the model behind the search form of `app\modules\admin\models\Disciplina`.
 */
class DisciplinaSearch extends Disciplina
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'credito', 'user_create', 'user_update'], 'integer'],
            [['nome', 'cod_cred', 'semestre', 'status', 'data_create', 'data_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Disciplina::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'credito' => $this->credito,
            'data_create' => $this->data_create,
            'user_create' => $this->user_create,
            'data_update' => $this->data_update,
            'user_update' => $this->user_update,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'cod_cred', $this->cod_cred])
            ->andFilterWhere(['like', 'semestre', $this->semestre])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
