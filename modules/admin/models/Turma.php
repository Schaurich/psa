<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "turma".
 *
 * @property int $id
 * @property int $numero_turma
 * @property string $data_create
 * @property int $user_create
 * @property string $data_update
 * @property int $user_update
 *
 * @property DisciplinaAlunoTurma[] $disciplinaAlunoTurmas
 * @property DisciplinaTurma[] $disciplinaTurmas
 * @property User $userCreate
 * @property User $userUpdate
 */
class Turma extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'turma';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_turma', 'user_create', 'user_update'], 'integer'],
            [['data_create', 'data_update'], 'safe'],
            [['user_create'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_create' => 'id']],
            [['user_update'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_update' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'numero_turma' => 'Numero Turma',
            'data_create' => 'Data Create',
            'user_create' => 'User Create',
            'data_update' => 'Data Update',
            'user_update' => 'User Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplinaAlunoTurmas()
    {
        return $this->hasMany(DisciplinaAlunoTurma::className(), ['id_turma' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplinaTurmas()
    {
        return $this->hasMany(DisciplinaTurma::className(), ['turma_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_update']);
    }
}
