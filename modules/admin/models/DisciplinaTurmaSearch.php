<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\DisciplinaTurma;

/**
 * DisciplinaTurmaSearch represents the model behind the search form of `app\modules\admin\models\DisciplinaTurma`.
 */
class DisciplinaTurmaSearch extends DisciplinaTurma
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'turma_id', 'disciplina_id', 'turno', 'user_create', 'user_update', 'vagas'], 'integer'],
            [['horario', 'data_create', 'data_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DisciplinaTurma::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'turma_id' => $this->turma_id,
            'disciplina_id' => $this->disciplina_id,
            'turno' => $this->turno,
            'user_create' => $this->user_create,
            'data_create' => $this->data_create,
            'user_update' => $this->user_update,
            'data_update' => $this->data_update,
            'vagas' => $this->vagas,
        ]);

        $query->andFilterWhere(['like', 'horario', $this->horario]);

        return $dataProvider;
    }
}
