<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\DisciplinaAlunoTurma;

/**
 * DisciplinaAlunoTurmaSearch represents the model behind the search form of `app\modules\admin\models\DisciplinaAlunoTurma`.
 */
class DisciplinaAlunoTurmaSearch extends DisciplinaAlunoTurma
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_disciplina', 'id_user', 'id_turma', 'user_create', 'user_update'], 'integer'],
            [['situacao', 'data_create', 'data_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DisciplinaAlunoTurma::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_disciplina' => $this->id_disciplina,
            'id_user' => $this->id_user,
            'id_turma' => $this->id_turma,
            'data_create' => $this->data_create,
            'user_create' => $this->user_create,
            'data_update' => $this->data_update,
            'user_update' => $this->user_update,
        ]);

        $query->andFilterWhere(['like', 'situacao', $this->situacao]);

        return $dataProvider;
    }
    public function search_mat($params)
    {
        $query = DisciplinaAlunoTurma::find()->where(['situacao'=>'MAT']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_disciplina' => $this->id_disciplina,
            'id_user' => $this->id_user,
            'id_turma' => $this->id_turma,
            'data_create' => $this->data_create,
            'user_create' => $this->user_create,
            'data_update' => $this->data_update,
            'user_update' => $this->user_update,
        ]);

        $query->andFilterWhere(['like', 'situacao', $this->situacao]);

        return $dataProvider;
    }
    
}
