<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\DisciplinaPreRequisito;

/**
 * DisciplinaPreRequisitoSearch represents the model behind the search form of `app\modules\admin\models\DisciplinaPreRequisito`.
 */
class DisciplinaPreRequisitoSearch extends DisciplinaPreRequisito
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'disciplina_id', 'pre_disciplina_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DisciplinaPreRequisito::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'disciplina_id' => $this->disciplina_id,
            'pre_disciplina_id' => $this->pre_disciplina_id,
        ]);

        return $dataProvider;
    }
}
