<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "disciplina_pre_requisito".
 *
 * @property int $id
 * @property int $disciplina_id
 * @property int $pre_disciplina_id
 *
 * @property Disciplina $disciplina
 * @property Disciplina $preDisciplina
 */
class DisciplinaPreRequisito extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'disciplina_pre_requisito';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['disciplina_id', 'pre_disciplina_id'], 'integer'],
            [['disciplina_id'], 'exist', 'skipOnError' => true, 'targetClass' => Disciplina::className(), 'targetAttribute' => ['disciplina_id' => 'id']],
            [['pre_disciplina_id'], 'exist', 'skipOnError' => true, 'targetClass' => Disciplina::className(), 'targetAttribute' => ['pre_disciplina_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'disciplina_id' => 'Disciplina ID',
            'pre_disciplina_id' => 'Pre Disciplina ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplina()
    {
        return $this->hasOne(Disciplina::className(), ['id' => 'disciplina_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreDisciplina()
    {
        return $this->hasOne(Disciplina::className(), ['id' => 'pre_disciplina_id']);
    }
}
