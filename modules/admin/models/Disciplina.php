<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "disciplina".
 *
 * @property int $id
 * @property string $nome
 * @property string $cod_cred
 * @property int $credito
 * @property string $semestre
 * @property string $status
 * @property string $data_create
 * @property int $user_create
 * @property string $data_update
 * @property int $user_update
 *
 * @property User $userCreate
 * @property User $userUpdate
 * @property DisciplinaAlunoTurma[] $disciplinaAlunoTurmas
 * @property DisciplinaPreRequisito[] $disciplinaPreRequisitos
 * @property DisciplinaPreRequisito[] $disciplinaPreRequisitos0
 * @property DisciplinaTurma[] $disciplinaTurmas
 */
class Disciplina extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'disciplina';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['credito', 'user_create', 'user_update'], 'integer'],
            [['data_create', 'data_update'], 'safe'],
            [['nome', 'cod_cred', 'semestre', 'status'], 'string', 'max' => 45],
            [['user_create'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_create' => 'id']],
            [['user_update'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_update' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'cod_cred' => 'Cod Cred',
            'credito' => 'Credito',
            'semestre' => 'Semestre',
            'status' => 'Status',
            'data_create' => 'Data Create',
            'user_create' => 'User Create',
            'data_update' => 'Data Update',
            'user_update' => 'User Update',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserCreate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_create']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserUpdate()
    {
        return $this->hasOne(User::className(), ['id' => 'user_update']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplinaAlunoTurmas()
    {
        return $this->hasMany(DisciplinaAlunoTurma::className(), ['id_disciplina' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplinaPreRequisitos()
    {
        return $this->hasMany(DisciplinaPreRequisito::className(), ['disciplina_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplinaPreRequisitos0()
    {
        return $this->hasMany(DisciplinaPreRequisito::className(), ['pre_disciplina_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisciplinaTurmas()
    {
        return $this->hasMany(DisciplinaTurma::className(), ['disciplina_id' => 'id']);
    }
}
