$(function () {

    $('#calendar').fullCalendar({
        height: 500,
        themeSystem: 'bootstrap4',
        slotEventOverlap: false,
        allDaySlot: false,
        eventSources: [
            // your event source
            {
                url: '/gsn/web/admin/compromisso/get_eventos/',
                type: 'GET',
                error: function () {
                    alert('there was an error while fetching events!');
                },
            }

            // any other sources...

        ],
        minTime: '08:00:00',
        maxTime: '18:00:00',
        locale: 'pt-br',
        defaultView: "agendaWeek",
        timezone: 'America/Sao_Paulo',
        header: {
            left: 'prev,next agendaDay,agendaWeek,',
            center: 'title',
            right: ''
        },
        eventMouseover: function (calEvent, jsEvent) {
            var title = calEvent.title;
            var d_start = calEvent.start.format("HH:mm");
            var d_end = calEvent.end.format("HH:mm");
            //console.log(calEvent);

            var tooltip = '<div class="tooltipevent" style="border-radius: 8px;width:130px;height:auto;background:#000;color:#FFF;opacity: 0.5;position:absolute;z-index:10001;"> <div class="col-md-6"><p>Título: </p></div><div class="col-md-6"><p>' + title + '</p></div><div class="col-md-6"><p>Início: </p></div><div class="col-md-6"><p>' + d_start + '</p></div><div class="col-md-6"><p>Fim: </p></div><div class="col-md-6"><p>' + d_end + '</p></div></div></div>';
            var $tooltip = $(tooltip).appendTo('body');

            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $tooltip.fadeIn('500');
                $tooltip.fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $tooltip.css('top', e.pageY + 10);
                $tooltip.css('left', e.pageX + 20);
            });
        },
        eventClick: function(event) {
                $('#visualizar #id').text(event.id);
                $('#visualizar #id').val(event.id);
                $('#visualizar #select2-compromisso-lead_id-container').text(event.lead_nome);
                $('#visualizar #compromisso-lead_id').val(event.lead_id);
                $('#visualizar #select2-compromisso-compromisso_tipo_id-container').text(event.compromisso_tipo_nome);
                $('#visualizar #compromisso-compromisso_tipo_id').val(event.compromisso_tipo_id);
                $('#visualizar #select2-compromisso-compromisso_status_id-container').text(event.compromisso_status_nome);
                $('#visualizar #compromisso-compromisso_status_id').val(event.compromisso_status_id);
                $('#visualizar #title').text(event.title);
                $('#visualizar #title').val(event.title);
                $('#visualizar #start').text(event.start.format('DD/MM/YYYY HH:mm'));
                $('#visualizar #start').val(event.start.format('DD/MM/YYYY HH:mm'));
                $('#visualizar #end').text(event.end.format('DD/MM/YYYY HH:mm'));
                $('#visualizar #end').val(event.end.format('DD/MM/YYYY HH:mm'));
                $('#visualizar #color').val(event.color);
                console.log(event);
                $('#visualizar').modal('show');
                return false;

        },
        selectable: true,
        selectHelper: true,
        select: function(start, end){
                $('#cadastrar #compromisso-compromisso_status_id').attr('style','display:show !important;');
                $('#cadastrar #compromisso-lead_id').attr('style','display:show !important;');
                $('#cadastrar #compromisso-compromisso_tipo_id').attr('style','display:show !important;');
                $('#cadastrar #start').val(moment(start).format('DD/MM/YYYY HH:mm'));
                $('#cadastrar #end').val(moment(end).format('DD/MM/YYYY HH:mm'));
                $('#cadastrar').modal('show');						
        },
        
        eventMouseout: function (calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.tooltipevent').remove();
        },
        eventDrop: function (event, delta, revertFunc) {

            if (confirm("Você deseja salvar o compromisso?")) {
                var data = 'id=' + event.id + '&start=' + event.start.format() + '&end=' + event.end.format();
                pageurl = '/gsn/web/admin/compromisso/update/?';
                $.ajax({
                    //url da pagina
                    url: pageurl + data,
                    //tipo: POST ou GET
                    type: 'POST',
                    //cache
                    cache: false,
                    //se ocorrer um erro na chamada ajax, retorna este alerta
                    //possiveis erros: pagina nao existe, erro de codigo na pagina, falha de comunicacao/internet, etc etc etc
                    error: function () {
                        window.alert('Error informar ti.');

                    },
                    //retorna o resultado da pagina para onde enviamos os dados
                    success: function ()
                    {
                        console.log("O seu registo foi inserido com sucesso!");
                    }
                });
            }
            else{
                revertFunc();
            }

        },
        eventResize: function (event, delta, revertFunc) {
            if (confirm("Você deseja salvar o compromisso?")) {

                var data = 'id=' + event.id + '&start=' + event.start.format() + '&end=' + event.end.format();
                pageurl = '/gsn/web/admin/compromisso/update/?';
                $.ajax({
                    //url da pagina
                    url: pageurl + data,
                    //tipo: POST ou GET
                    type: 'POST',
                    //cache
                    cache: false,
                    //se ocorrer um erro na chamada ajax, retorna este alerta
                    //possiveis erros: pagina nao existe, erro de codigo na pagina, falha de comunicacao/internet, etc etc etc
                    error: function () {
                        window.alert('Error informar ti.');

                    },
                    //retorna o resultado da pagina para onde enviamos os dados
                    success: function ()
                    {
                        console.log("O seu registo foi inserido com sucesso!");
                    }
                });
            } else {
                revertFunc();
            }

        },
        editable: true, // don't allow event dragging
        eventResourceEditable: false, // except for between resources
        eventLimit: true, // allow "more" link when too many events

    });

});

$('#calendar').height($(window).height());
function DataHora(evento, objeto){
    var keypress=(window.event)?event.keyCode:evento.which;
    campo = eval (objeto);
    if (campo.value == '00/00/0000 00:00'){
            campo.value=""
    }

    caracteres = '0123456789';
    separacao1 = '/';
    separacao2 = ' ';
    separacao3 = ':';
    conjunto1 = 2;
    conjunto2 = 5;
    conjunto3 = 10;
    conjunto4 = 13;
    conjunto5 = 16;
    if ((caracteres.search(String.fromCharCode (keypress))!=-1) && campo.value.length <= (15)){
            if (campo.value.length == conjunto1 )
            campo.value = campo.value + separacao1;
            else if (campo.value.length == conjunto2)
            campo.value = campo.value + separacao1;
            else if (campo.value.length == conjunto3)
            campo.value = campo.value + separacao2;
            else if (campo.value.length == conjunto4)
            campo.value = campo.value + separacao3;
            else if (campo.value.length == conjunto5)
            campo.value = campo.value + separacao3;
    }else{
            event.returnValue = false;
    }
}
$('.btn-canc-vis').on("click", function() {
        $('.form').slideToggle();
        $('.visualizar').slideToggle();
});
$('.btn-canc-edit').on("click", function() {
        $('.visualizar').slideToggle();
        $('.form').slideToggle();
});