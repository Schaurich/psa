<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <!--<link rel="icon" href="../../minimalliteadmin/images/favicon.ico">-->
        <link rel="icon" href="../../images/psa_matriculas.jpg">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition login-page">
            <?php $this->beginBody() ?>
        <div class="mx-auto col-md-4" style="z-index: 999;float: right;">
            <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success'); ?></div>
            <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error'); ?></div>
            <?php elseif (Yii::$app->session->hasFlash('info')): ?>
                <div class="alert alert-info"><?= Yii::$app->session->getFlash('info'); ?></div>
        <?php endif; ?>                              
        </div>
<?= $content ?>
<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>