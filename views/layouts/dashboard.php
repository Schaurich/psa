<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\DashboardAsset;

DashboardAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="icon" href="../../minimalliteadmin/images/favicon.ico">-->
        <link rel="icon" href="../../images/psa_matriculas.jpg">
        <title><?= Html::encode($this->title) ?></title>
    </head>    
    <?php $this->head() ?>

    <body class="hold-transition skin-green sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrapper">
            <?= Yii::$app->controller->renderPartial('/psa/_header'); ?>
            <?= Yii::$app->controller->renderPartial('/psa/_menuleft'); ?>
            <div class="mx-auto col-md-4" style="z-index: 999;float: right;">
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success"><?= Yii::$app->session->getFlash('success'); ?></div>
                <?php elseif (Yii::$app->session->hasFlash('error')): ?>
                    <div class="alert alert-danger"><?= Yii::$app->session->getFlash('error'); ?></div>
                <?php elseif (Yii::$app->session->hasFlash('info')): ?>
                    <div class="alert alert-info"><?= Yii::$app->session->getFlash('info'); ?></div>
                <?php endif; ?>                              
            </div>
            <?= $content ?>
            <?= Yii::$app->controller->renderPartial('/psa/_footer'); ?>
            <?= Yii::$app->controller->renderPartial('/psa/_menuright'); ?>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>